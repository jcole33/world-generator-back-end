module.exports = {
	root: true,

	parserOptions: {
		parser: "@typescript-eslint/parser",
		sourceType: "module"
	},

	env: {
		browser: true
	},

	extends: [
		"plugin:@typescript-eslint/recommended",
		"plugin:prettier/recommended"
	],

	plugins: ["prettier", "@typescript-eslint", "import"],
	globals: {
		ga: true, // Google Analytics
		cordova: true,
		__statics: true,
		process: true
	},

	// add your custom rules here
	rules: {
		indent: ["error", "tab"],
		"no-mixed-spaces-and-tabs": "error",
		"no-tabs": ["error", { allowIndentationTabs: true }],
		"@typescript-eslint/indent": "off",
		"@typescript-eslint/explicit-function-return-type": [
			"error",
			{ allowExpressions: true }
		],
		"@typescript-eslint/no-explicit-any": "off",
		semi: "off",
		"@typescript-eslint/semi": ["error", "never"],
		"@typescript-eslint/member-delimiter-style": [
			"error",
			{
				singleline: {
					delimiter: "semi",
					requireLast: false
				},
				multiline: {
					delimiter: "none",
					requireLast: false
				}
			}
		],
		"@typescript-eslint/explicit-member-accessibility": [
			"error",
			{ accessibility: "no-public" }
		],
		"@typescript-eslint/adjacent-overload-signatures": "off",
		// allow async-await
		"generator-star-spacing": "off",
		// allow paren-less arrow functions
		"arrow-parens": "off",
		"one-var": "off",
		"import/first": "off",
		"import/named": "error",
		"import/namespace": "error",
		"import/default": "error",
		"@typescript-eslint/no-unused-vars": [
			"error",
			{ argsIgnorePattern: "^_" }
		],
		"@typescript-eslint/generic-type-naming": ["error", "^[A-Z][a-zA-Z]+$"],
		"@typescript-eslint/no-parameter-properties": [
			"error",
			{ allows: ["private"] }
		],
		"import/export": "error",
		"import/extensions": "off",
		"import/no-unresolved": "off",
		"import/no-extraneous-dependencies": "off",
		"prefer-promise-reject-errors": "off",

		// allow console.log during development only
		"no-console": process.env.NODE_ENV === "production" ? "error" : "off",
		// allow debugger during development only
		"no-debugger": process.env.NODE_ENV === "production" ? "error" : "off"
	}
}
