import { NextFunction, Request, Response } from "express"

type TypeOfTypes = "string" | "number" | "bigint" | "boolean" | "symbol" | "undefined" | "object" | "function" | "array"

interface KeyValuePair<O> {
    key: keyof O
    value: O[keyof O]
}

type CheckWrapperConfig<P extends Record<string, any>, B extends Record<string, any>> = {
    params?: Record<keyof P, "string">
    body?: Record<keyof B, TypeOfTypes>
}

function checkProperties<O extends Record<string, any>>(declaredProperties: KeyValuePair<Record<keyof O, TypeOfTypes>>[] | KeyValuePair<Record<keyof O, "string">>[], requestBody: Partial<O>, objectName: string) {
	for (let i = 0; i < declaredProperties.length; ++i) {
		const propertyName = declaredProperties[i].key
		const propertyType = declaredProperties[i].value
		if (!requestBody[propertyName]) return "Missing " + objectName + " property '" + propertyName + "'" 
		if (
            (
                propertyType != "array" && typeof requestBody[propertyName] != propertyType
            ) ||
            (
                propertyType == "array" && (
                    typeof requestBody[propertyName] != "object" || !Array.isArray(requestBody[propertyName])
                )
            )
        ) return "Invalid type for " + objectName + " property '" + propertyName + "'" 
		if (propertyType == "string" && requestBody[propertyName] == '') return "Empty string for " + objectName + " property '" + propertyName + "'" 
	}
	return true
}

function keyValuePairs<O>(object: O): KeyValuePair<O>[] {
    const list: KeyValuePair<O>[] = []
    for (const key in object) {
        list.push({
            key: key,
            value: object[key]
        })
    }   
    return list
}

export function createCheckWrapper<P extends Record<string, string> = Record<never, never>, B extends Record<string, any> = Record<never, never>>(checkWrapperConfig: CheckWrapperConfig<P, B>) {
    const paramPropertyDeclarations = checkWrapperConfig.params ? keyValuePairs(checkWrapperConfig.params) : []
    const bodyPropertyDeclarations = checkWrapperConfig.body ? keyValuePairs(checkWrapperConfig.body) : []
    //for some reason request gets mad at P unless it is partial, still enforces type safety in practice so
    return function(req: Request<Partial<P>, {message: string}, B>, res: Response<{message: string}>, next: NextFunction) {
        if (checkWrapperConfig.params) {
            const paramCheckResponse = checkProperties(paramPropertyDeclarations, req.params, "parameter")
            if (typeof paramCheckResponse == "string") return res.status(400).json({message: paramCheckResponse})
        }
        if (checkWrapperConfig.body) {
            const bodyCheckResponse = checkProperties(bodyPropertyDeclarations, req.body, "body")
            if (typeof bodyCheckResponse == "string") return res.status(400).json({message: bodyCheckResponse})
        }
        next()
    }
}