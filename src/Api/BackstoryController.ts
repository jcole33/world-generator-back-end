import { Controller, ClassOptions, ClassWrapper, Post } from "@overnightjs/core"
import { Request, Response } from "express"
import expressAsyncHandler from "express-async-handler"
import {openai} from "../OpenAI"

@Controller("backstory")
@ClassWrapper(expressAsyncHandler)
@ClassOptions({ mergeParams: true })
export class BackstoryController {
	@Post()
	private async post(req: Request<{seed: string, type: string}>, res: Response): Promise<Response> {
		const prompt = "Create a short backstory for a " + req.body.type + " with the following attributes: " + req.body.seed
		const response = await openai.createCompletion("text-davinci-001", {
			prompt: prompt,
			temperature: 0.9,
			max_tokens: 100,
			top_p: 1.0,
			frequency_penalty: 1.5,
			presence_penalty: 1.5	
		})
		if (response.data.choices)
			return res.status(200).json({backstory: response.data.choices[0].text, prompt: prompt})
		return res.status(500).json({error: "No content generated"})
	}
}
