import { Controller, ClassOptions, Get, ClassWrapper, Post, Put, Wrapper, Middleware } from "@overnightjs/core"
import pool from "../db"
import { Request, Response } from "express"
import expressAsyncHandler from "express-async-handler"
import {uuid} from "../regex"
import { attributesTemplates } from "../data/AttributeTemplates"
import {Attribute, Generator} from "../types/models"
import { createCheckWrapper } from "../wrapper"

type PostBody = {name: string, type: string}

@Controller("generator")
@ClassWrapper(expressAsyncHandler)
@ClassOptions({ mergeParams: true })
export class GeneratorController {
	@Get(":id(" + uuid + ")")
	@Middleware([createCheckWrapper<{id: string}>({
		params: {
			id: "string"
		}
	})])
	private async get(req: Request<{id: string}>, res: Response<Generator | {message: string}>): Promise<Response<Generator | {message: string}>> {
		const response = await pool.query<Generator>('SELECT id, type, name, attributes FROM generator WHERE id = $1::uuid', [req.params.id])
		if (response.rowCount > 0)
			return res.status(200).json(response.rows[0])
		return res.status(404).json({message: "failure"})
	}

	@Get("templates")
	private async getTemplates(req: Request<{id: string}>, res: Response<string[]>): Promise<Response<string[]>> {
		return res.status(200).json(Object.keys(attributesTemplates))
	}

	@Put(":id(" + uuid + ")")
	@Middleware([createCheckWrapper<{id: string}, {name: string, attributes: Attribute[]}>({
		params:{
			id: "string"
		},
		body: {
			name: "string",
			attributes: "array"
		}
	})])
	private async put(req: Request<{id: string}, {message: string}, {name: string, attributes: Attribute[]}>, res: Response<{message: string}>): Promise<Response> {
		await pool.query('UPDATE generator SET name = $2, attributes = $3::jsonb[] WHERE id = $1::uuid', [req.params.id, req.body.name, req.body.attributes])
		return res.status(200).json({ message: "success"})
	}
	
	@Post()
	@Middleware([createCheckWrapper<{}, PostBody>({
		body: {
			name: "string",
			type: "string"
		}
	})])
	private async post(req: Request<never, {id: string}, PostBody>, res: Response<{id: string}>): Promise<Response> {
		
		const name = req.body.name
		const type = req.body.type.toLowerCase()
		//@ts-ignore
		const attributes: Attribute[] = req.body.type in attributesTemplates ? attributesTemplates[type] : []
		const response = await pool.query<{id: string}>('INSERT INTO generator(name, type, attributes) VALUES($1, $2, $3::jsonb[]) RETURNING id', [name, type, attributes])
		return res.status(200).json(response.rows[0])
	}
}
