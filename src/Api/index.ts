import {
	Controller,
	ClassOptions,
	ChildControllers,
	ClassWrapper
} from "@overnightjs/core"

import expressAsyncHandler from "express-async-handler"
import { GeneratorController } from "./GeneratorController"
import {BackstoryController} from "./BackstoryController"

@Controller("api")
@ClassWrapper(expressAsyncHandler)
@ClassOptions({ mergeParams: true })
@ChildControllers([new GeneratorController(), new BackstoryController()])
export class Api {}
