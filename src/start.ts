// Import and start Server. Remember, server must
// be imported after configuring env variables
import { App } from "./App"

let server = new App()
const envPort = process.env.PORT
let port = 8000
if (envPort != undefined)
	port = parseInt(envPort)
server.start(port)
