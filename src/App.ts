//import express from "express"
import { Server } from "@overnightjs/core"
import { Api } from "./Api"
import bodyParser from "body-parser"
import cors from "cors"

export class App extends Server {
	constructor() {
		super(process.env.NODE_ENV === "development")
		this.app.use(cors({
			origin: ["http://localhost:8080", "http://generate-world.s3-website-us-west-1.amazonaws.com", "http://computational-creative-demo.s3-website-us-east-1.amazonaws.com"],
			methods: "GET,HEAD,PUT,PATCH,POST,DELETE"
		}))
		this.app.use(bodyParser.json())
		this.app.use(bodyParser.urlencoded({ extended: true }))
		/*this.app.use(
			express.static(
				process.env.frontFolder ? process.env.frontFolder : ""
			)
		)*/
		this.setupControllers()
	}
	private setupControllers(): void {
		const api = new Api()
		super.addControllers([api])
	}
	start(port: number): void {
		this.app.listen(port, () => {
			console.log('Server listening on port:' + port)

		})
	}
}
