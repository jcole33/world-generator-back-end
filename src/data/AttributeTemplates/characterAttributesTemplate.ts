import { Attribute } from '../../types/models';

const characterAttributesTemplate: Attribute[] = [
  {
    name: 'Background',
    options: [
      {
        name: 'War Veteran'
      },
      {
        name: 'Student'
      },
      {
        name: 'Noble'
      },
      {
        name: 'Temple Attendant'
      },
      {
        name: 'Performer'
      },
      {
        name: 'Charlatan'
      },
      {
        name: 'Criminal'
      },
      {
        name: 'Guild Artisan'
      },
      {
        name: 'Hermit'
      },
      {
        name: 'Scholar'
      },
      {
        name: 'Sailor'
      },
      {
        name: 'City Watch'
      },
      {
        name: 'Athlete'
      },
      {
        name: 'Archaeologist'
      },
      {
        name: 'Gladiator'
      },
      {
        name: 'Spy'
      },
      {
        name: 'Smuggler'
      },
      {
        name: 'Investigator'
      },
      {
        name: 'Knight'
      },
      {
        name: 'Pirate'
      },
      {
        name: 'Bounty Hunter'
      }
    ],
    uniform: true
  },
  {
    name: 'Personality',
    options: [
      {
        name: 'Pessimistic'
      },
      {
        name: 'Optimistic'
      },
      {
        name: 'Hardworking'
      },
      {
        name: 'Mean'
      },
      {
        name: 'Loyal'
      },
      {
        name: 'Stubborn'
      },
      {
        name: 'Selfish'
      },
      {
        name: 'Brilliant'
      },
      {
        name: 'Creative'
      },
      {
        name: 'Adventurous'
      },
      {
        name: 'Carefree'
      },
      {
        name: 'Studious'
      },
      {
        name: 'Inventive'
      },
      {
        name: 'Mischievous'
      },
      {
        name: 'Charming'
      },
      {
        name: 'Daring'
      },
      {
        name: 'Patriotic'
      },
      {
        name: 'Responsible'
      },
      {
        name: 'Cautious'
      },
      {
        name: 'Lazy'
      },
      {
        name: 'Conceited'
      },
      {
        name: 'Lucky'
      },
      {
        name: 'Ambitious'
      },
      {
        name: 'Hopeful'
      },
      {
        name: 'Mysterious'
      },
      {
        name: 'Quiet'
      },
      {
        name: 'Curious'
      },
      {
        name: 'Energetic'
      },
      {
        name: 'Witty'
      },
      {
        name: 'Rude'
      },
      {
        name: 'Strict'
      },
      {
        name: 'Foolish'
      },
      {
        name: 'Naive'
      },
      {
        name: 'Grumpy'
      },
      {
        name: 'Sly'
      },
      {
        name: 'Thrifty'
      },
      {
        name: 'Talented'
      },
      {
        name: 'Wise'
      },
      {
        name: 'Timid'
      },
      {
        name: 'Shy'
      }
    ],
    uniform: true
  },
  {
    name: 'Profession',
    options: [
      {
        name: 'Farmer'
      },
      {
        name: 'Wizard'
      },
      {
        name: 'Monk'
      },
      {
        name: 'Rogue'
      },
      {
        name: 'Blacksmith'
      },
      {
        name: 'Adventurer'
      },
      {
        name: 'Soldier'
      },
      {
        name: 'Treasure Hunter'
      },
      {
        name: 'Warrior'
      },
      {
        name: 'Druid'
      },
      {
        name: 'Barbarian'
      },
      {
        name: 'Bard'
      },
      {
        name: 'Cleric'
      },
      {
        name: 'Paladin'
      },
      {
        name: 'Ranger'
      },
      {
        name: 'Sorcerer'
      },
      {
        name: 'Warlock'
      },
      {
        name: 'Herbalist'
      },
      {
        name: 'Shopkeeper'
      },
      {
        name: 'War Veteran'
      },
      {
        name: 'Student'
      },
      {
        name: 'Noble'
      },
      {
        name: 'Temple Attendant'
      },
      {
        name: 'Performer'
      },
      {
        name: 'Charlatan'
      },
      {
        name: 'Criminal'
      },
      {
        name: 'Guild Artisan'
      },
      {
        name: 'Hermit'
      },
      {
        name: 'Scholar'
      },
      {
        name: 'Sailor'
      },
      {
        name: 'City Watch'
      },
      {
        name: 'Athlete'
      },
      {
        name: 'Archaeologist'
      },
      {
        name: 'Gladiator'
      },
      {
        name: 'Spy'
      },
      {
        name: 'Smuggler'
      },
      {
        name: 'Investigator'
      },
      {
        name: 'Knight'
      },
      {
        name: 'Pirate'
      },
      {
        name: 'Bounty Hunter'
      }
    ],
    uniform: true
  },
  {
    name: 'Gender',
    options: [
      {
        name: 'Male'
      },
      {
        name: 'Female'
      },
      {
        name: 'Non-binary'
      }
    ],
    uniform: true
  },
  {
    name: 'Economic Class',
    options: [
      {
        name: 'Poor'
      },
      {
        name: 'Middle Class'
      },
      {
        name: 'Rich'
      }
    ],
    uniform: true
  },
  {
    name: 'Hobby',
    options: [
      {
        name: 'Woodworking'
      },
      {
        name: 'Painting'
      },
      {
        name: 'Writing'
      },
      {
        name: 'Cooking'
      },
      {
        name: 'Surfing'
      },
      {
        name: 'Dancing'
      },
      {
        name: 'Gardening'
      },
      {
        name: 'Singing'
      },
      {
        name: 'Board Games'
      },
      {
        name: 'Hiking'
      },
      {
        name: 'Pottery'
      },
      {
        name: 'Sewing'
      }
    ],
    uniform: true
  },
  {
    name: 'Age',
    options: [
      {
        name: 'Child'
      },
      {
        name: 'Adolescent'
      },
      {
        name: 'Adult'
      },
      {
        name: 'Middle-age'
      },
      {
        name: 'Old'
      },
      {
        name: 'Ancient'
      }
    ],
    uniform: true
  },
  {
    name: 'Height',
    options: [
      {
        name: 'Miniscule'
      },
      {
        name: 'Short'
      },
      {
        name: 'Tall'
      },
      {
        name: 'Towering'
      },
      {
        name: 'Average'
      }
    ],
    uniform: true
  }
];

export default characterAttributesTemplate;
