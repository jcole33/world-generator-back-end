import { getTokenSourceMapRange } from 'typescript'
import { Attribute } from '../../types/models';

const planetAttributesTemplate: Attribute[] = [
    {
        name: "Life Population",
        uniform: true,
        options: [
            {
                name: "desolate"
            },
            {
                name: "small"
            },
            {
                name: "sizeable"
            },
            {
                name: "thriving"
            },
            {
                name: "garden world"
            }
        ]
    },
    {
        name: "Environment",
        uniform: true,
        options: [
            {
                name: "ocean world"
            },
            {
                name: "desert"
            },
            {
                name: "salt flats"
            },
            {
                name: "ice world"
            },
            {
                name: "storm"
            }
        ]
    },
    {
        name: "Average Temperature",
        uniform: true,
        options: [
            {
                name: "freezing"
            },
            {
                name: "cold"
            },
            {
                name: "temperate"
            },
            {
                name: "hot"
            },
            {
                name: "boiling"
            }
        ]
    },
    {
        name: "Day Length",
        uniform: true,
        options: [
            {
                name: "short"
            },
            {
                name: "medium"
            },
            {
                name: "long"
            },
            {
                name: "tidally-locked"
            }
        ]
    },
    {
        name: "Size",
        uniform: true,
        options: [
            {
                name: "dwarf planet"
            },
            {
                name: "small"
            },
            {
                name: "average"
            },
            {
                name: "large"
            }
        ]
    }
]

export default planetAttributesTemplate