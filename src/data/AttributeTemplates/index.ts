import characterAttributesTemplate from './characterAttributesTemplate'
import townAttributesTemplate from './townAttributesTemplate'
import planetAttributesTemplate from "./planetAttributesTemplate"
import shopAttributesTemplate from './shopAttributesTemplate'
const attributesTemplates =  {
    character: characterAttributesTemplate,
    town: townAttributesTemplate,
    planet: planetAttributesTemplate,
    shop: shopAttributesTemplate
}
export {attributesTemplates}
