import { Attribute } from '../../types/models';
const townAttributesTemplate: Attribute[] = [
  {
    name: 'Population Size',
    options: [
      {
        name: 'Abandoned'
      },
      {
        name: 'Small'
      },
      {
        name: 'Average'
      },
      {
        name: 'Large'
      },
      {
        name: 'Crowded'
      }
    ],
    uniform: true
  },
  {
    name: 'Main Export',
    options: [
      {
        name: 'Ore'
      },
      {
        name: 'Stone'
      },
      {
        name: 'Produce'
      },
      {
        name: 'Animals'
      },
      {
        name: 'Crafted Goods'
      },
      {
        name: 'Gold'
      },
      {
        name: 'Meat'
      },
      {
        name: 'Medicinal Herbs'
      }
    ],
    uniform: true
  },
  {
    name: 'Climate',
    options: [
      {
        name: 'Tropical'
      },
      {
        name: 'Desert'
      },
      {
        name: 'Temperate'
      },
      {
        name: 'Mediterranean'
      },
      {
        name: 'Polar'
      }
    ],
    uniform: true
  },
  {
    name: 'Political Structure',
    options: [
      {
        name: 'Town Council'
      },
      {
        name: 'Duchy'
      },
      {
        name: 'Oligarchy'
      },
      {
        name: 'Communist'
      },
      {
        name: 'Democracy'
      }
    ],
    uniform: true
  },
  {
    name: 'Average Education',
    options: [
      {
        name: 'None'
      },
      {
        name: 'Some'
      },
      {
        name: 'Moderate'
      },
      {
        name: 'Great'
      }
    ],
    uniform: true
  },
  {
    name: 'Economic Class',
    options: [
      {
        name: 'Squalid'
      },
      {
        name: 'Poor'
      },
      {
        name: 'Middle Class'
      },
      {
        name: 'Rich'
      }
    ],
    uniform: true
  },
  {
    name: 'Average Disposition',
    options: [
      {
        name: 'Welcoming'
      },
      {
        name: 'Distrusting'
      },
      {
        name: 'Helpful'
      },
      {
        name: 'Selfish'
      }
    ],
    uniform: true
  }
];

export default townAttributesTemplate;
