import { Attribute } from '../../types/models';

const shopAttributesTemplate: Attribute[] = [
    {
        name: "Store Type",
        uniform: true,
        options: [
            {
                name: "weapons"
            },
            {
                name: "general goods"
            },
            {
                name: "herbs"
            },
            {
                name: "potions"
            },
            {
                name: "armor"
            },
            {
                name: "baked goods"
            },
            {
                name: "specialty (magic or rare technology)"
            },
            {
                name: "clothing"
            },
            {
                name: "carfting supplies"
            }
        ]
    },
    {
        name: "size",
        uniform: true,
        options: [
            {
                name: "small"
            },
            {
                name: "medium"
            },
            {
                name: "large"
            },
            {
                name: "emporium"
            }
        ]
    },
    {
        name: "Owner Personality",
        uniform: true,
        options: [
            {
                name: "friendly",
            },
            {
                name: "grumpy"
            },
            {
                name: "thrifty"
            },
            {
                name: "miserly"
            },
            {
                name: "depressed"
            },
            {
                name: "exhausted"
            },
            {
                name: "happy"
            },
            {
                name: "timid"
            },
            {
                name: "helpful"
            }
        ]
    },
    {
        name: "Organization",
        uniform: true,
        options: [
            {
                name: "spotless"
            },
            {
                name: "organized"
            },
            {
                name: "disorganized"
            },
            {
                name: "scattered"
            }
        ]
    },
    {
        name: "Store Age",
        uniform: true,
        options: [
            {
                name: "new"
            },
            {
                name: "established"
            },
            {
                name: "ancient"
            }
        ]
    },
    {
        name: "Product Quality",
        uniform: true,
        options: [
            {
                name: "poor"
            },
            {
                name: "acceptable"
            },
            {
                name: "average"
            },
            {
                name: "good"
            },
            {
                name: "incredible"
            }
        ]
    }
]

export default shopAttributesTemplate