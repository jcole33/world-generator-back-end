export interface Option {
    name: string
    probability?: number
}

export interface Attribute {
    name: string
    uniform: boolean
    options: Option[]
}


export interface Generator {
    id: Readonly<string>
    type: string
    name: string
    attributes: Attribute[]
}