FROM node:16
VOLUME /tmp

COPY dist/ /app/

RUN cd app && npm i --only=prod
CMD ["node", "app/src/start.js"]
